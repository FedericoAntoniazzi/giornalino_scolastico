<?php

session_start();

require_once "php/components/navbar.php";
require_once "php/articoli.php"

?>

<!DOCTYPE html>
<html lang="it">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">

    <title>Homepage</title>

    <link rel="stylesheet" href="css/bootstrap.min.css">
</head>
<body>

    <?php echo getNavbar(ActiveNavButton::HOME); ?>

    <div class="container">

        <!-- intestazione -->
        <section class="jumbotron text-center">
            <h1 class="jumbotron-heading">Giornalino scolastico</h1>
            <p class="lead text-muted">
                <?php
                if(isset($_SESSION['email'])){
                    echo "Benvenuto, ".$_SESSION['email']." (".$_SESSION['tipoAccount'].")";
                }
                ?>
                <br>
                Consulta il giornalino scolastico!
                <br>
                <br>
                Se sei un autore accedi per pubblicare degli articoli!
            </p>
        </section>

        <!-- Articoli -->
        <div class="row">
            <?php echo getArticoli(); ?>
        </div>
    </div>

    <script src="https://code.jquery.com/jquery-3.2.1.slim.min.js"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.12.9/umd/popper.min.js"></script>
    <script src="js/bootstrap.min.js"></script>
</body>
</html>