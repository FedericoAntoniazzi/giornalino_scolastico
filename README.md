# Giornalino scolastico

## Prefazione
Un gruppo di studenti intende realizzare un giornale scolastico in cui pubblicare articoli inerenti alle attività didattiche.

## Analisi
### Articoli
Ogni notizia deve avere un titolo, un breve riassunto, il nome dell’autore e il testo completo dell’articolo
Inoltre l’articolo deve essere associato a delle hotwords (hashtag) e ad una determinata categoria.

Le notizie inviate dagli autori devono essere sottoposte ad approvazione per essere pubblicati.

### Utenti
Il sito dovrà implementare 4 tipologie di utenti:
- **Lettore**: Può leggere gli articoli pubblicati
- **Scrittore**: Può leggere e scrivere articoli ma non pubblicarli
- **Revisore**: Può leggere, scrivere e approvare articoli per la pubblicazione
- **Amministratori**: Ha il pieno controllo del sistema. A livello di permessi è come un Revisore ma può anche aggiungere, disabilitare e cambiare i permessi degli utenti

## Progettazione del database

### Diagramma ER
![ER Diagram](sql/er_diagram.png)

### Schema logico
Account(**idAccount**, nome, cognome, email, password, dataIscrizione, dataScadenza, _tipoAccount_)

TipoAccount(**idTipoAccount**, tipo, descrizione)

Categorie(**idCategoria**, categoria)

Articoli(**idArticolo**, titolo, sottotitolo, testo, dataCreazione, hotwords, _autore_, _categoria_)

Revisione(**idRevisione**, _articolo_, _revisione_)

StatoRevisione(**idStatoRevisione**, stato, descrizione)

### Schema fisico
![Physic data model](sql/physical_data_model.png)

## Utenti e permessi
See [users.sql](https://gitlab.com/FedericoAntoniazzi/giornalino_scolastico/-/blob/master/sql/users.sql)

## Procedure

### Login
Il login può essere effettuato in qualsiasi momento tramite email e password.
Il tipo di account viene rilevato automaticamente

URL: /php/components/login.php

### Visualizzazione degli articoli
Gli articoli APPROVATI possono essere letti da chiunque in quanto viene usato l’account lettore (default).

URL: /index.php

### Scrittura degli articoli
Gli articoli possono essere scritti solo da scrittore, revisore e amministratore.
Un articolo è composto da Titolo (TINYTEXT), Sottotitolo(TINYTEXT), Categoria (chiave esterna della tabella Categorie), testo (TEXT) e hotwords (TEXT).

URL: /php/write_article.php

### Revisione degli articoli
I revisori e gli amministratori possono approvare gli articoli nella schermata apposita grazie al pulsante Approva e Rifiuta

URL: /php/approve.php

### Aggiungere utenti
La registrazione di nuovi utenti può essere eseguita solo dall’amministratore che provvederà ad inserire nome, cognome, email, password e conferma della password.

URL: /php/components/add_user.php

### Modificare i permessi degli utenti
L’amministratore può modificare i permessi degli utenti in maniera tale da assegnare un ruolo come scrittore, lettore, revisore o amministratore.
ATTENZIONE: L’amministratore attivo non visualizza le sue informazioni per motivi di sicurezza

URL: /php/edit_users.php

