<?php

require_once "database.php";
require_once "navbar.php";

session_start();

if(isset($_SESSION['idAccount']) && $_SESSION['is_logged']===true){
    header("location: http://localhost:8080/index.php");
    exit();
}

$email = $password = $email_error = $password_error = $general_error = "";

if($_SERVER["REQUEST_METHOD"] == "POST"){

    /* Controlli sull'email */
    // Non deve essere vuoto
    if(empty(trim($_POST['email']))){
        $email_error = "Il campo non può essere vuoto";
    } else {
        $email= $_POST['email'];
    }

    /* Controlli sulla password */
    // Non deve essere vuoto
    if(empty(trim($_POST['password']))){
        $password_error = "Il campo non può essere vuoto";
    } else {
        $password = $_POST['password'];
    }

    // Entra solo se non ci sono stati problemi con le credenziali
    if(empty($password_error) && empty($email_error)){
        $query = "SELECT * FROM Account ".
            "JOIN TipiAccount TA on Account.tipoAccount = TA.idTipoAccount ".
            "WHERE email=\"%s\" AND dataScadenza IS NULL";
        $query = sprintf($query, $email);
        error_log($query);
        $result = execSQL($query, 'login_helper');

        if(!$result) {
            $general_error = "Errore di connessione al db";
            error_log("Errore di connessione al db");
        } else if(!$result->num_rows) {
            $general_error = "Credenziali non valide";
            error_log("Credenziali non valide");
            error_log("".$result->num_rows);
        } else {
            $result = $result->fetch_assoc();
            $hash = hash('sha256', $password);
            if($hash === $result['password']){
                $_SESSION['is_logged'] = true;
                $_SESSION['idAccount'] = $result['idAccount'];
                $_SESSION['email'] = $result['email'];
                $_SESSION['tipoAccount'] = $result['tipo'];
                error_log("Login effettuato con successo: ".$result['email']);
                header("location: http://localhost:8080/index.php");
                exit();
            } else {
                $general_error = "Credenziali non valide";
            }
        }
    }
}

?>

<!doctype html>
<html lang="it">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">

    <title>Login</title>

    <link rel="stylesheet" href="../../css/bootstrap.min.css">
</head>
<body>
    <?php echo getNavbar(ActiveNavButton::LOGIN) ?>

    <div class="p-5"></div>

    <div class="align-content-center">
        <div class="card container text-center col-md-4 p-4">
            <h5 class="text-danger"><?php echo $general_error?></h5>
            <form method="post" action="<?php echo htmlspecialchars($_SERVER['PHP_SELF']); ?>">
                <div class="form-group">
                    <label for="email">Email</label>
                    <input type="email" class="form-control" id="email" name="email" placeholder="Inserisci email">
                    <small id="email_error" class="form-text text-danger"><?php echo $email_error ?></small>
                </div>
                <div class="form-group">
                    <label for="password">Password</label>
                    <input type="password" class="form-control" id="password" name="password" placeholder="Inserisci password">
                    <small id="email_error" class="form-text text-danger"><?php echo $password_error ?></small>
                </div>
                <button type="submit" class="btn btn-primary">Log in</button>
            </form>
        </div>
    </div>
</body>
</html>
