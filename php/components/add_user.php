<?php

require_once "database.php";
require_once "navbar.php";

session_start();

if(!isset($_SESSION['tipoAccount']) || $_SESSION['tipoAccount'] != 'amministratore'){
    header("location: http://localhost:8080/index.php");
    exit();
}

$name = $surname = $email = $password = $name_error = $surname_error = "";
$email_error = $password_error = $general_error = $password_confirm_error = "";

if($_SERVER["REQUEST_METHOD"] == "POST"){

    $field_cannot_be_empty = "Il campo non può essere vuoto";

    /* Controlli sull'email */
    // Non deve essere vuoto
    if(empty(trim($_POST['email']))){
        $email_error = $field_cannot_be_empty;
    } else {
        $email= $_POST['email'];
    }

    /* Controlli sulla password */
    // Non deve essere vuoto
    if(empty(trim($_POST['password']))){
        $password_error = $field_cannot_be_empty;
    } else if(empty(trim($_POST['password_confirm']))){
        $password_confirm_error = $field_cannot_be_empty;
    } else if($_POST['password'] == $_POST['password_confirm']){
        $password = $_POST['password'];
    } else {
        $password_confirm_error = "Le password non corrispondono";
    }

    /* Controlli sul nome */
    if(empty(trim($_POST['name']))){
        $name_error = $field_cannot_be_empty;
    } else {
        $name = $_POST['name'];
    }

    /* Controlli su  cognome */
    if(empty(trim($_POST['surname']))){
        $surname_error = $field_cannot_be_empty;
    } else {
        $surname = $_POST['surname'];
    }

    // Entra solo se non ci sono stati problemi con le credenziali
    if(empty($password_error) && empty($email_error) &&
        empty($password_confirm_error) && empty($name_error) && empty($surname_error)){



        $lettore = "SELECT * FROM TipiAccount WHERE tipo='lettore'";
        $lettore = execSQL($lettore, $_SESSION['tipoAccount'])->fetch_assoc();
        $lettore = $lettore['idTipoAccount'];

        $password = hash('sha256', $password);

        $create_user = "INSERT INTO Account (nome, cognome, email, password, dataIscrizione, tipoAccount) 
                            VALUES ('$name', '$surname', '$email', '$password', CURDATE(), $lettore)";

        $res = execSQL($create_user, $_SESSION['tipoAccount']);
        if($res === true) {
            error_log("Account registrato con successo");
            header("Location: /index.php");
        } else {
            error_log("Errore nella registrazione dell'account");
            $general_error = "C'è stato un errore nella creazione dell'account. \n";
            $general_error .= "Si prega di controllare che la mail non sia già registrata";
        }
    }
}

?>

<!doctype html>
<html lang="it">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">

    <title>Nuovo utente</title>

    <link rel="stylesheet" href="../../css/bootstrap.min.css">
</head>
<body>
    <?php echo getNavbar(ActiveNavButton::EDIT_USERS) ?>

    <div class="p-5"></div>

    <div class="align-content-center text-center ">
        <h4>Aggiungi un nuovo utente</h4>
        <br>
        <div class="card container col-md-4 p-4">
            <h5 class="text-danger"><?php echo $general_error?></h5>
            <form method="post" action="<?php echo htmlspecialchars($_SERVER['PHP_SELF']); ?>">
                <div class="form-group">
                    <label for="name">Nome</label>
                    <input type="text" class="form-control" id="name" name="name" placeholder="Inserisci nome">
                    <small id="name_error" class="form-text text-danger"><?php echo $name_error ?></small>
                </div>
                <div class="form-group">
                    <label for="surname">Cognome</label>
                    <input type="text" class="form-control" id="surname" name="surname" placeholder="Inserisci cognome">
                    <small id="surname_error" class="form-text text-danger"><?php echo $surname_error ?></small>
                </div>
                <div class="form-group">
                    <label for="email">Email</label>
                    <input type="email" class="form-control" id="email" name="email" placeholder="Inserisci email">
                    <small id="email_error" class="form-text text-danger"><?php echo $email_error ?></small>
                </div>
                <div class="form-group">
                    <label for="password">Password</label>
                    <input type="password" class="form-control" id="password" name="password" placeholder="Inserisci password">
                    <small id="password_error" class="form-text text-danger"><?php echo $password_error ?></small>
                </div>
                <div class="form-group">
                    <label for="password_confirm">Conferma password</label>
                    <input type="password" class="form-control" id="password_confirm" name="password_confirm" placeholder="Inserisci password">
                    <small id="password_confirm_error" class="form-text text-danger"><?php echo $password_confirm_error ?></small>
                </div>
                <button type="submit" class="btn btn-outline-success">Registra</button>
            </form>
        </div>
    </div>
</body>
</html>
