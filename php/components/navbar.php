<?php

/* Comodo per richiamare in maniera più semplice la pagina attiva */
class ActiveNavButton {
    const HOME = 0;
    const LOGIN = 1;
    const LOGOUT = 2;
    const WRITE_ARTICLE = 3;
    const APPROVE_ARTICLES = 4;
    const EDIT_USERS = 5;
}

/* Ritorna la navbar. I pulsanti sono già mappati alla rispettiva pagina */
function getNavbar(String $active) {

    $logged = false;
    if(isset($_SESSION['is_logged'])){
        $logged = $_SESSION['is_logged'];
    }

    $reader_buttons = array(
        ActiveNavButton::HOME => "Homepage",
        ActiveNavButton::LOGIN => "Login",
    );

    $author_buttons = array(
        ActiveNavButton::LOGOUT => "Esci",
        ActiveNavButton::WRITE_ARTICLE => "Scrivi un articolo",
    );
    $author_buttons = array_merge($reader_buttons, $author_buttons);

    $reviser_buttons = array(
        ActiveNavButton::APPROVE_ARTICLES => "Approva un articolo",
    );
    $reviser_buttons = array_merge($author_buttons, $reviser_buttons);

    $admin_buttons = array(
        ActiveNavButton::EDIT_USERS => "Modifica utenti",
    );
    $admin_buttons = array_merge($reviser_buttons, $admin_buttons);

    $links = array(
        ActiveNavButton::HOME => "http://localhost:8080/index.php",
        ActiveNavButton::LOGIN => "http://localhost:8080/php/components/login.php",
        ActiveNavButton::LOGOUT => "http://localhost:8080/php/components/logout.php",
        ActiveNavButton::WRITE_ARTICLE => "http://localhost:8080/php/write_article.php",
        ActiveNavButton::APPROVE_ARTICLES => "http://localhost:8080/php/approve.php",
        ActiveNavButton::EDIT_USERS => "http://localhost:8080/php/edit_users.php",
    );

    $navbar = "<nav class=\"navbar navbar-expand-lg navbar-light bg-light sticky-top\">";

    // Titolo della navbar
    $navbar .= "<a class=\"navbar-brand\" href=\"http://localhost:8080/index.php\">Giornalino scolastico</a>";

    // Pulsanti
    $navbar .= "<ul class=\"nav nav-pills justify-content-center\">";

    $buttons = "";

    if($logged){
        if($_SESSION['tipoAccount'] == "scrittore"){
            $buttons = $author_buttons;
        } else if($_SESSION['tipoAccount'] == "revisore"){
            $buttons = $reviser_buttons;
        } else if($_SESSION['tipoAccount'] == "amministratore"){
            $buttons = $admin_buttons;
        }
    } else {
        $buttons = $reader_buttons;
    }

    foreach ($buttons as $title => $url){
        if($logged && $title==ActiveNavButton::LOGIN) {
            continue;
        }
        $navbar .= "<li class=\"nav-item\">";
        $navbar .= "<a class=\"nav-link ";

        if($title == $active) {
            $navbar .= "active";
        }

        $navbar .= "\" href=\"".$links[$title]."\">$url</a>";
        $navbar .= "</li>";
    }

    $navbar .= "</ul></nav>";

    return $navbar;
}