<?php

function _openConnection($user, $password="password") {
    $dbHost = "localhost";
    $dbName = "giornale";

    return new mysqli($dbHost, $user, $password, $dbName);
}

function _closeConnection(mysqli $conn) {
    $conn->close();
}

function execSQL(string $query, string $user, string $password="password") {
    $conn = _openConnection($user, $password);
    $result = $conn->query($query);
    _closeConnection($conn);
    return $result;
}
