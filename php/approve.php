<?php

session_start();

include_once "components/navbar.php";
include_once "components/database.php";

$message = "";

if($_SERVER['REQUEST_METHOD'] == "POST") {
    $stato = "";
    $idArticolo = -4;
    // Risponde il pulsante giusto
    if(isset($_POST['approva'])){
        $approvato = "SELECT * FROM StatoRevisione WHERE stato='approvato'";
        $stato = execSQL($approvato, $_SESSION['tipoAccount'])->fetch_assoc()['idStatoRevisione'];
        $idArticolo = $_POST['approva'];
    } else if(isset($_POST['rifiuta'])) {
        $rifiutato = "SELECT * FROM StatoRevisione WHERE stato='rifiutato'";
        $stato = execSQL($rifiutato, $_SESSION['tipoAccount'])->fetch_assoc()['idStatoRevisione'];
        $idArticolo = $_POST['rifiuta'];
    }

    $query = "INSERT INTO Revisione (articolo, stato) VALUES ($idArticolo, $stato)";

    $res = execSQL($query, $_SESSION['tipoAccount']);
    if($res === true) {
        error_log("Articolo revisionato con successo");
    } else {
        error_log("Errore nella revisione");
    }
}

function getArticlesToRevise(){
    $query = "SELECT idArticolo, idAccount, titolo, sottotitolo, testo, dataCreazione, hotwords, C.categoria, nome, cognome
FROM Articoli art
JOIN Categorie C ON art.categoria = C.idCategoria
JOIN Account A on art.autore = A.idAccount
WHERE art.idArticolo NOT IN (
    SELECT articolo
    FROM Revisione
)
ORDER BY dataCreazione";

    $result = execSQL($query, $_SESSION['tipoAccount']);

    if(!$result) {
        return "Errore di connessione al DB";
    }

    if(!$result->num_rows){
        return "<div class='w-100 p-3 text-center'>Non ci sono articoli da approvare</div>";
    }

    $articles = "";

    while($r=$result->fetch_assoc()){
        $date = date("d/m/Y", strtotime($r['dataCreazione']));
        $self = htmlspecialchars($_SERVER['PHP_SELF']);
        $articles .= <<<HTML
            <div class="col-md-6">
                <div class="card">
                    <div class="card-header">
                        <small class="text-muted">{$r["categoria"]}</small>
                    </div>
                    <div class="card-body">
                        <h3 class="card-title">{$r['titolo']}</h3>
                        <h4 class="card-subtitle">{$r['sottotitolo']}</h4>
                        <p class="card-text">{$r['testo']}</p>
                    </div>
                    <div class="card-footer">
                        <small class="text-muted">Pubblicato da {$r['nome']} {$r['cognome']} il $date</small>
                        <form method="post" action="{$_SERVER['PHP_SELF']}">
                        <br>
                            <div class="form-group">
                                <button type="submit" class="btn btn-success" name="approva" value="{$r['idArticolo']}">Approva</button>
                                <button type="submit" class="btn btn-outline-danger" name="rifiuta" value="{$r['idArticolo']}">Rifiuta</button>
                            </div>
                        </form>
                    </div>
                </div>
            </div>
HTML;
    }

    return $articles;
}

function getDiscardedArticles(){
    $query = "SELECT idArticolo, idAccount, titolo, sottotitolo, testo, dataCreazione, hotwords, C.categoria, nome, cognome
FROM Articoli art
JOIN Categorie C ON art.categoria = C.idCategoria
JOIN Account A on art.autore = A.idAccount
JOIN Revisione R on art.idArticolo = R.articolo
JOIN StatoRevisione SR on R.stato = SR.idStatoRevisione
WHERE SR.stato='rifiutato'
ORDER BY dataCreazione";

    $result = execSQL($query, $_SESSION['tipoAccount']);

    if(!$result) {
        return "Errore di connessione al DB";
    }

    if(!$result->num_rows){
        return "<div class='w-100 p-3 text-center'>Non ci sono articoli rifiutati</div>";
    }

    $articles = "";

    while($r=$result->fetch_assoc()){
        $date = date("d/m/Y", strtotime($r['dataCreazione']));
        $self = htmlspecialchars($_SERVER['PHP_SELF']);
        $articles .= <<<HTML
            <div class="col-md-6">
                <div class="card">
                    <div class="card-header" style="background-color: #ffb5b5">
                        <small class="text-muted">{$r["categoria"]}</small>
                    </div>
                    <div class="card-body">
                        <h3 class="card-title">{$r['titolo']}</h3>
                        <h4 class="card-subtitle">{$r['sottotitolo']}</h4>
                        <p class="card-text">{$r['testo']}</p>
                    </div>
                    <div class="card-footer">
                        <small class="text-muted">Pubblicato da {$r['nome']} {$r['cognome']} il $date</small>
                        <form method="post" action="{$_SERVER['PHP_SELF']}">
                        <br>
                            <div class="form-group">
                                <button type="submit" class="btn btn-success" name="approva" value="{$r['idArticolo']}">Approva</button>
                                <button type="submit" class="btn btn-outline-danger" name="rifiuta" value="{$r['idArticolo']}">Rifiuta</button>
                            </div>
                        </form>
                    </div>
                </div>
            </div>
HTML;
    }

    return $articles;
}

?>

<!DOCTYPE html>
<html lang="it">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">

    <title>Approva gli articoli</title>

    <link rel="stylesheet" href="../css/bootstrap.min.css">
</head>
<body>

    <?php echo getNavbar(ActiveNavButton::APPROVE_ARTICLES); ?>

    <div class="container text-center">

        <!-- intestazione -->
        <section class="jumbotron text-center">
            <h1 class="jumbotron-heading">Approva articoli</h1>
            <p class="lead text-muted">
                Approva gli articoli proposti dagli autori
            </p>
        </section>

        <h2>Articoli da approvare</h2>
        <div class="row text-left">
            <?php echo getArticlesToRevise(); ?>
        </div>

        <div class="p-3"></div>

        <h2>Articoli rifiutati</h2>
        <div class="row text-left">
            <?php echo getDiscardedArticles(); ?>
        </div>
    </div>

    <div class="p-3"></div>

    <script src="https://code.jquery.com/jquery-3.2.1.slim.min.js"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.12.9/umd/popper.min.js"></script>
    <script src="../js/bootstrap.min.js"></script>
</body>
</html>