<?php

session_start();

include_once "components/navbar.php";
include_once "components/database.php";

$general_error = "";

if($_SERVER["REQUEST_METHOD"] == "POST"){

    // Non ci devono essere campi vuoti (hotwords possono essere vuote)
    if(empty(trim($_POST['titolo'])) || empty(trim($_POST['sottotitolo'])) ||
            empty(trim($_POST['testo']))){
        $general_error = "Non ci possono essere campi vuoti";
    } else {

        $conn = _openConnection($_SESSION['tipoAccount']);
        $_POST['titolo'] = $conn->real_escape_string($_POST['titolo']);
        $_POST['sottotitolo'] = $conn->real_escape_string($_POST['sottotitolo']);
        $_POST['hotwords'] = $conn->real_escape_string($_POST['hotwords']);
        _closeConnection($conn);


        $insert = "INSERT INTO Articoli (titolo, sottotitolo, testo, dataCreazione, autore, categoria, hotwords) ".
            "VALUES (\"".trim($_POST['titolo'])."\", \"".trim($_POST['sottotitolo'])."\", ".
            "\"".trim($_POST['testo'])."\", CURDATE(), ".$_SESSION['idAccount'].", ".$_POST['categoria'].", \"".$_POST['hotwords']."\");";

        error_log($insert);

        $res = execSQL($insert, $_SESSION['tipoAccount']);
        if($res === true){
            error_log("Articolo inserito con successo");
        } else {
            error_log("Errore nell'inserimento ");
        }
    }
}

function categories_combobox_items(){
    $query = "SELECT * FROM Categorie";
    $result = execSQL($query, $_SESSION['tipoAccount']);
    error_log($_SESSION['tipoAccount']);
    $output = "";
    while($row=$result->fetch_assoc()){
        $output .= "<option value=\"".$row['idCategoria']."\">";
        $output .= $row['categoria'];
        $output .= "</option>";
    }
    error_log($output);
    return $output;
}

?>

<!doctype html>
<html lang="it">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">

    <title>Scrivi un articolo</title>

    <link rel="stylesheet" href="../css/bootstrap.min.css">
</head>
<body>
    <?php echo getNavbar(ActiveNavButton::WRITE_ARTICLE) ?>
    <div class="container">
        <section class="jumbotron text-center">
            <h1 class="jumbotron-heading">Scrivi un articolo</h1>
            <br>
            <p class="lead text-muted">
                Ricorda di inserire informazioni vere e realmente accadute senza offendere o insultare nessuno.
            </p>
        </section>
    </div>
    <div class="align-content-center">
        <div class="card container col-md-6 p-4">
            <h5 class="text-danger"><?php echo $general_error; ?></h5>
            <form method="post" action="<?php echo htmlspecialchars($_SERVER['PHP_SELF']); ?>">
                <div class="form-group">
                    <label for="titolo">Titolo</label>
                    <input type="text" class="form-control" id="titolo" name="titolo" placeholder="Inserisci il titolo dell'articolo">
                </div>
                <div class="form-group">
                    <label for="sottotitolo">Sottotitolo</label>
                    <input type="text" class="form-control" id="sottotitolo" name="sottotitolo" placeholder="Inserisci sottotitolo">
                </div>
                <div class="form-group">
                    <label for="testo">Testo</label>
                    <textarea class="form-control" id="testo" name="testo" rows="10" placeholder="Scrivi il testo dell'articolo"></textarea>
                </div>
                <div class="form-group">
                    <label for="categoria">Categoria</label>
                    <select id="categoria" name="categoria" class="form-control">
                        <?php echo categories_combobox_items(); ?>
                    </select>
                </div>
                <div class="form-group">
                    <label for="hotwords">Hotwords</label>
                    <textarea class="form-control" name="hotwords" id="hotwords" rows="2" placeholder="Scrivi le hotwords dell'articolo"></textarea>
                </div>
                <button type="submit" class="btn btn-primary">Invia</button>
            </form>
        </div>
    </div>
    <div class="p-3"></div>

</body>
</html>
