<?php

require_once "components/database.php";

function getArticoli() {
    $query = "SELECT * FROM Revisione ".
        "JOIN Articoli A ON Revisione.articolo = A.idArticolo ".
        "JOIN StatoRevisione SR ON Revisione.stato = SR.idStatoRevisione ".
        "JOIN Categorie ON Categorie.idCategoria = A.categoria ".
        "WHERE SR.stato = 'approvato' ".
        "ORDER BY dataCreazione DESC";
    error_log($query);
    $result = execSQL($query, 'lettore');

    if(!$result) {
        return "Errore di connessione al DB";
    }

    if(!$result->num_rows){
        return "<div class='w-100 p-3 text-center'>Non ci sono articoli disponibili</div>";
    }

    $articoli = "";

    while($r=$result->fetch_array(MYSQLI_ASSOC)){
        $date = date("d/m/Y", strtotime($r['dataCreazione']));
        $articoli .= "<div class=\"col-md-6\"> <div class=\"card\"> ";
        $articoli .= "<div class=\"card-header\">";
        $articoli .= "<small class=\"text-muted\">".$r['categoria']."</small><br>";
        $articoli .= "<small class='text-muted'>$date</small></div>";
        $articoli .= "<div class=\"card-body\">";
        $articoli .= "<h3 class=\"card-title\">".$r['titolo']."</h3>";
        $articoli .= "<h5 class=\"card-title\">".$r['sottotitolo']."</h5>";
        $articoli .= "<p class=\"card-text\">".$r['testo']."</p>";
        $articoli .= "</div></div></div>";
    }

    return $articoli;
}