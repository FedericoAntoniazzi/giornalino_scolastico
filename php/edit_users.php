<?php

require_once "components/navbar.php";
require_once "components/database.php";

session_start();

if(!isset($_SESSION['tipoAccount']) || $_SESSION['tipoAccount'] != 'amministratore'){
    header("location: http://localhost:8080/index.php");
    exit();
}

$countTipiAccount = "SELECT COUNT(*) FROM TipiAccount";
$countTipiAccount = execSQL($countTipiAccount, $_SESSION['tipoAccount'])->fetch_row()[0];

if($_SERVER["REQUEST_METHOD"] == "POST"){

    /* Abilita l'utente */
    if(isset($_POST['attiva'])){
        $query = "UPDATE Account SET dataScadenza = NULL WHERE idAccount = {$_POST['attiva']}";
        execSQL($query, $_SESSION['tipoAccount']);
        error_log($_POST['attiva']." attivato");

    /* Disabilita l'utente */
    } else if(isset($_POST['disattiva'])){
        $query = "UPDATE Account SET dataScadenza = CURDATE() WHERE idAccount = {$_POST['disattiva']}";
        execSQL($query, $_SESSION['tipoAccount']);
        error_log($_POST['disattiva']." disattivato");

    /* Promuovo o declasse l'utente changeRole */
    } else if(isset($_POST['changeRole'])){

        // Tipo account attuale
        $query = "SELECT * FROM TipiAccount JOIN Account A on TipiAccount.idTipoAccount = A.tipoAccount 
                WHERE A.idAccount = '{$_POST['changeRole']}'";
        $current_tipoAccount = execSQL($query, $_SESSION['tipoAccount'])->fetch_assoc();

        // TipoAccount Promosso
        $next_tipoAccount = ($current_tipoAccount['idTipoAccount'] % $countTipiAccount) + 1;
        $query = "SELECT * FROM TipiAccount WHERE idTipoAccount = $next_tipoAccount";
        $next_tipoAccount = execSQL($query, $_SESSION['tipoAccount'])->fetch_assoc();

        $query = "UPDATE Account SET tipoAccount = {$next_tipoAccount['idTipoAccount']} WHERE idAccount = {$_POST['changeRole']}";
        $res = execSQL($query, $_SESSION['tipoAccount']);
        if($res === true) {
            error_log("Account modificato con successo");
        } else {
            error_log("Errore nella modifica dell'account");
        }
    }
}

// Calcola il livello successivo
// Ritorna il pulsante con "Promuovi a " oppure "Declassa a "
function nextLevel($email){
    global $countTipiAccount;

    // Tipo account attuale
    $query = "SELECT * FROM TipiAccount JOIN Account A on TipiAccount.idTipoAccount = A.tipoAccount 
                WHERE A.email = '$email'";
    $current_tipoAccount = execSQL($query, $_SESSION['tipoAccount'])->fetch_assoc();

    // TipoAccount Promosso
    $next_tipoAccount = ($current_tipoAccount['idTipoAccount'] % $countTipiAccount) + 1;
    $query = "SELECT * FROM TipiAccount WHERE idTipoAccount = $next_tipoAccount";
    $next_tipoAccount = execSQL($query, $_SESSION['tipoAccount'])->fetch_assoc();

    $change_to = "Promuovi a ";

    if($next_tipoAccount['idTipoAccount'] < $current_tipoAccount['idTipoAccount']){
        $change_to = "Declassa a ";
    }

    return <<<HTML
<button type="submit" class="btn btn-warning" value='{$current_tipoAccount['idAccount']}' name='changeRole'>{$change_to} {$next_tipoAccount['tipo']}</button>
HTML;

}

function activate_or_deactivate($account){
    if(empty($account['dataScadenza'])){
        return "<button type=\"submit\" class=\"btn btn-danger\" value='{$account['idAccount']}' name='disattiva'>Disabilita</button>";
    }
    return "<button type=\"submit\" class=\"btn btn-success\" value='{$account['idAccount']}' name='attiva'>Abilita</button>";
}

function showUsersWithMenu() {
    $output = "";

    // Evita che l'amministratore si declassi da solo
    $query = "select idAccount, nome, cognome, email, dataScadenza, tipo from Account join TipiAccount TA on Account.tipoAccount = TA.idTipoAccount where idAccount <> {$_SESSION['idAccount']} order by dataScadenza, nome, cognome;";
    $users = execSQL($query, $_SESSION['tipoAccount']);

    while($r=$users->fetch_assoc()){
        $output .= <<<HTML
<div class="card text-center">
    <div class="card-body row justify-content-between">
        <div class="justify-content-center align-self-center">
            <p class="card-text">{$r['nome']} {$r['cognome']} ({$r['email']}) [{$r['tipo']}]</p>
        </div>
        <form class="btn-group" role="group" method="post" action="{$_SERVER['PHP_SELF']}">
HTML;
        $output .= nextLevel($r['email']);
        $output .= activate_or_deactivate($r);
        $output .= <<<HTML
        </form>
    </div>
</div>
HTML;
    }
    return $output;
}

?>

<!doctype html>
<html lang="it">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">

    <title>Modifica utenti</title>

    <link rel="stylesheet" href="../css/bootstrap.min.css">
</head>
<body>

    <?php echo getNavbar(ActiveNavButton::EDIT_USERS) ?>

    <div class="container text-center">

        <!-- intestazione -->
        <section class="jumbotron text-center">
            <h1 class="jumbotron-heading">Modifica utenti</h1>
            <p class="lead text-muted">
                Modifica i permessi degli utenti
            </p>
        </section>

        <div class="container">
            <?php echo showUsersWithMenu(); ?>
            <!-- Aggiungere un nuovo utente -->
            <div class="card text-center">
                <div class="card-body row justify-content-between">
                    <div class="justify-content-center align-self-center">
                        <p>Aggiungi un nuovo utente</p>
                    </div>
                    <div class="btn-group" role="group">
                        <a href="components/add_user.php" role="button" class="btn btn-outline-primary" >Nuovo utente</a>
                    </div>
                </div>
            </div>
        </div>
    </div>

    <script src="https://code.jquery.com/jquery-3.2.1.slim.min.js"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.12.9/umd/popper.min.js"></script>
    <script src="../js/bootstrap.min.js"></script>
</body>
</html>
