CREATE USER IF NOT EXISTS 'login_helper'@'localhost' IDENTIFIED BY 'password';
REVOKE ALL PRIVILEGES ON *.* FROM 'login_helper'@'localhost';
GRANT SELECT ON giornale.Account TO 'login_helper'@'localhost';
GRANT SELECT ON giornale.TipiAccount TO 'login_helper'@'localhost';


CREATE USER IF NOT EXISTS 'lettore'@'localhost' IDENTIFIED BY 'password';
REVOKE ALL PRIVILEGES ON *.* FROM 'lettore'@'localhost';
GRANT SELECT ON giornale.Revisione TO 'lettore'@'localhost';
GRANT SELECT ON giornale.Articoli TO 'lettore'@'localhost';
GRANT SELECT ON giornale.Categorie TO 'lettore'@'localhost';
GRANT SELECT ON giornale.StatoRevisione TO 'lettore'@'localhost';


CREATE USER IF NOT EXISTS 'scrittore'@'localhost' IDENTIFIED BY 'password';
REVOKE ALL PRIVILEGES ON *.* FROM 'scrittore'@'localhost';
GRANT SELECT ON giornale.Revisione TO 'scrittore'@'localhost';
GRANT SELECT ON giornale.StatoRevisione TO 'scrittore'@'localhost';
GRANT SELECT, INSERT, UPDATE ON giornale.Articoli TO 'scrittore'@'localhost';
GRANT SELECT ON giornale.Categorie TO 'scrittore'@'localhost';


CREATE USER IF NOT EXISTS 'revisore'@'localhost' IDENTIFIED BY 'password';
REVOKE ALL PRIVILEGES ON *.* FROM 'revisore'@'localhost';
GRANT SELECT, INSERT, UPDATE ON giornale.Revisione TO 'revisore'@'localhost';
GRANT SELECT ON giornale.Account TO 'revisore'@'localhost';
GRANT SELECT ON giornale.StatoRevisione TO 'revisore'@'localhost';
GRANT SELECT ON giornale.Categorie TO 'revisore'@'localhost';
GRANT SELECT, INSERT, UPDATE ON giornale.Articoli TO 'revisore'@'localhost';


CREATE USER IF NOT EXISTS 'amministratore'@'localhost' IDENTIFIED BY 'password';
REVOKE ALL PRIVILEGES ON *.* FROM 'amministratore'@'localhost';
GRANT SELECT, INSERT, UPDATE ON *.* TO 'amministratore'@'localhost';


FLUSH PRIVILEGES;
