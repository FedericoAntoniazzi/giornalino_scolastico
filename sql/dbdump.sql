-- MariaDB dump 10.17  Distrib 10.4.12-MariaDB, for Linux (x86_64)
--
-- Host: localhost    Database: giornale
-- ------------------------------------------------------
-- Server version	10.4.12-MariaDB

/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;
/*!40103 SET @OLD_TIME_ZONE=@@TIME_ZONE */;
/*!40103 SET TIME_ZONE='+00:00' */;
/*!40014 SET @OLD_UNIQUE_CHECKS=@@UNIQUE_CHECKS, UNIQUE_CHECKS=0 */;
/*!40014 SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0 */;
/*!40101 SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='NO_AUTO_VALUE_ON_ZERO' */;
/*!40111 SET @OLD_SQL_NOTES=@@SQL_NOTES, SQL_NOTES=0 */;

--
-- Table structure for table `Account`
--

DROP TABLE IF EXISTS `Account`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `Account` (
  `idAccount` int(11) NOT NULL AUTO_INCREMENT,
  `nome` varchar(20) COLLATE utf8mb4_unicode_ci NOT NULL,
  `cognome` varchar(20) COLLATE utf8mb4_unicode_ci NOT NULL,
  `email` varchar(50) COLLATE utf8mb4_unicode_ci NOT NULL,
  `password` char(64) COLLATE utf8mb4_unicode_ci NOT NULL,
  `dataIscrizione` date NOT NULL,
  `dataScadenza` date DEFAULT NULL,
  `tipoAccount` int(11) NOT NULL,
  PRIMARY KEY (`idAccount`),
  UNIQUE KEY `Account_email_uindex` (`email`),
  KEY `tipoAccount` (`tipoAccount`),
  CONSTRAINT `Account_ibfk_1` FOREIGN KEY (`tipoAccount`) REFERENCES `TipiAccount` (`idTipoAccount`)
) ENGINE=InnoDB AUTO_INCREMENT=8 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `Account`
--

LOCK TABLES `Account` WRITE;
/*!40000 ALTER TABLE `Account` DISABLE KEYS */;
INSERT INTO `Account` VALUES (1,'Giulio','Rossi','giuliorossi@gmail.com','5e884898da28047151d0e56f8dc6292773603d0d6aabbdd62a11ef721d1542d8','2020-03-30',NULL,4),(2,'Luca','Gialli','lucagialli@gmail.com','5e884898da28047151d0e56f8dc6292773603d0d6aabbdd62a11ef721d1542d8','2020-03-30',NULL,3),(3,'Antonio','Verdi','antonioverdi@gmail.com','5e884898da28047151d0e56f8dc6292773603d0d6aabbdd62a11ef721d1542d8','2020-03-30',NULL,2),(5,'Federico','Antoniazzi','federico.antoniazzi.000@gmail.com','5e884898da28047151d0e56f8dc6292773603d0d6aabbdd62a11ef721d1542d8','2020-04-07',NULL,1),(6,'Mario','Neri','marioneri@gmail.com','5e884898da28047151d0e56f8dc6292773603d0d6aabbdd62a11ef721d1542d8','2020-04-07','2020-04-07',1),(7,'Pippo','Pippo','pippo@gmail.com','5e884898da28047151d0e56f8dc6292773603d0d6aabbdd62a11ef721d1542d8','2020-04-07',NULL,2);
/*!40000 ALTER TABLE `Account` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `Articoli`
--

DROP TABLE IF EXISTS `Articoli`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `Articoli` (
  `idArticolo` int(11) NOT NULL AUTO_INCREMENT,
  `titolo` tinytext COLLATE utf8mb4_unicode_ci NOT NULL,
  `sottotitolo` tinytext COLLATE utf8mb4_unicode_ci NOT NULL,
  `testo` text COLLATE utf8mb4_unicode_ci NOT NULL,
  `dataCreazione` date NOT NULL,
  `autore` int(11) NOT NULL,
  `categoria` int(11) NOT NULL,
  `hotwords` text COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  PRIMARY KEY (`idArticolo`),
  KEY `autore` (`autore`),
  KEY `categoria` (`categoria`),
  CONSTRAINT `Articoli_ibfk_1` FOREIGN KEY (`autore`) REFERENCES `Account` (`idAccount`),
  CONSTRAINT `Articoli_ibfk_2` FOREIGN KEY (`categoria`) REFERENCES `Categorie` (`idCategoria`)
) ENGINE=InnoDB AUTO_INCREMENT=8 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `Articoli`
--

LOCK TABLES `Articoli` WRITE;
/*!40000 ALTER TABLE `Articoli` DISABLE KEYS */;
INSERT INTO `Articoli` VALUES (1,'Salvini accoglie migranti per immunità di gregge','Si giustifica: \"L\'hanno detto i virologi\"','A quanto pare l\'ex premier vuole comprare tantissimi migranti per sconfiggere il coronavirus grazie all\'immunità di gregge.','2020-03-31',3,3,'salvini pagliaccio covid19 coronavirus'),(2,'Scuole chiuse fino a fine Luglio','Le tristi parole del ministro Azzolina','Eh sì, magari! Magari anche il 70 politico','2020-03-31',3,2,'azzolina coronavirus covid19 scuola scuole'),(3,'Tennista vince la coppa più bella degli altri','Gli avversari: \"È troppo fortunato\"','È successo veramente! Mario Mariello è arrivato primo al torneo di tennis di Wii Sport e ha vinto una coppa!','2020-03-25',2,4,'tennis wii coppa vincita vincere'),(4,'Festa a Napoli durante la quarantena. 50 denunciati','La polizia ha trovato i video pubblicati in rete','Successo sabato verso sera in un quartiere di Napoli','2020-03-31',2,1,'napoli festa quarantena coronavirus'),(5,'Scoperto libro del 1700','Anticipa il coronavirus','Trovato nel comune di Bugliano (PI) un libro che ha anticipato per fila e per segno il virus cinese nato a Wuhan a novembre del 2019','2020-04-07',1,3,'coronavirus libro bugliano'),(6,'euygfusyg','eiufhsiufh','seiufisuefesfsefsef','2020-04-07',1,1,'ef  sfs fef sf '),(7,'Pippo','Pippo','Pippo','2020-04-07',7,6,'pippo pluto topolino');
/*!40000 ALTER TABLE `Articoli` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `Categorie`
--

DROP TABLE IF EXISTS `Categorie`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `Categorie` (
  `idCategoria` int(11) NOT NULL AUTO_INCREMENT,
  `categoria` varchar(50) COLLATE utf8mb4_unicode_ci NOT NULL,
  PRIMARY KEY (`idCategoria`)
) ENGINE=InnoDB AUTO_INCREMENT=7 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `Categorie`
--

LOCK TABLES `Categorie` WRITE;
/*!40000 ALTER TABLE `Categorie` DISABLE KEYS */;
INSERT INTO `Categorie` VALUES (1,'Politica'),(2,'Scuola'),(3,'Sanità'),(4,'Sport'),(5,'Cronaca'),(6,'Attualità');
/*!40000 ALTER TABLE `Categorie` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `Revisione`
--

DROP TABLE IF EXISTS `Revisione`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `Revisione` (
  `idRevisione` int(11) NOT NULL AUTO_INCREMENT,
  `articolo` int(11) NOT NULL,
  `stato` int(11) NOT NULL,
  PRIMARY KEY (`idRevisione`),
  KEY `articolo` (`articolo`),
  KEY `stato` (`stato`),
  CONSTRAINT `Revisione_ibfk_1` FOREIGN KEY (`articolo`) REFERENCES `Articoli` (`idArticolo`),
  CONSTRAINT `Revisione_ibfk_2` FOREIGN KEY (`stato`) REFERENCES `StatoRevisione` (`idStatoRevisione`)
) ENGINE=InnoDB AUTO_INCREMENT=8 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `Revisione`
--

LOCK TABLES `Revisione` WRITE;
/*!40000 ALTER TABLE `Revisione` DISABLE KEYS */;
INSERT INTO `Revisione` VALUES (1,3,1),(2,2,1),(3,1,2),(4,6,2),(5,7,2),(6,6,2),(7,7,1);
/*!40000 ALTER TABLE `Revisione` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `StatoRevisione`
--

DROP TABLE IF EXISTS `StatoRevisione`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `StatoRevisione` (
  `idStatoRevisione` int(11) NOT NULL AUTO_INCREMENT,
  `stato` varchar(30) COLLATE utf8mb4_unicode_ci NOT NULL,
  `descrizione` tinytext COLLATE utf8mb4_unicode_ci NOT NULL,
  PRIMARY KEY (`idStatoRevisione`)
) ENGINE=InnoDB AUTO_INCREMENT=3 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `StatoRevisione`
--

LOCK TABLES `StatoRevisione` WRITE;
/*!40000 ALTER TABLE `StatoRevisione` DISABLE KEYS */;
INSERT INTO `StatoRevisione` VALUES (1,'approvato','L\'articolo è idoneo alla pubblicazione'),(2,'rifiutato','L\'articolo presenta contenuto offensivo o grammaticamente scorretto');
/*!40000 ALTER TABLE `StatoRevisione` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `TipiAccount`
--

DROP TABLE IF EXISTS `TipiAccount`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `TipiAccount` (
  `idTipoAccount` int(11) NOT NULL AUTO_INCREMENT,
  `tipo` varchar(50) COLLATE utf8mb4_unicode_ci NOT NULL,
  `descrizione` tinytext COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  PRIMARY KEY (`idTipoAccount`)
) ENGINE=InnoDB AUTO_INCREMENT=5 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `TipiAccount`
--

LOCK TABLES `TipiAccount` WRITE;
/*!40000 ALTER TABLE `TipiAccount` DISABLE KEYS */;
INSERT INTO `TipiAccount` VALUES (1,'lettore','Può solo leggere gli articoli'),(2,'scrittore','Lettore che può scrivere articoli'),(3,'revisore','Autore che può anche approvare gli articoli proposti dai revisori'),(4,'amministratore','Pieno controllo del sistema');
/*!40000 ALTER TABLE `TipiAccount` ENABLE KEYS */;
UNLOCK TABLES;
/*!40103 SET TIME_ZONE=@OLD_TIME_ZONE */;

/*!40101 SET SQL_MODE=@OLD_SQL_MODE */;
/*!40014 SET FOREIGN_KEY_CHECKS=@OLD_FOREIGN_KEY_CHECKS */;
/*!40014 SET UNIQUE_CHECKS=@OLD_UNIQUE_CHECKS */;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
/*!40111 SET SQL_NOTES=@OLD_SQL_NOTES */;

-- Dump completed on 2020-04-09 21:04:51
