CREATE TABLE IF NOT EXISTS TipiAccount (
    idTipoAccount INT PRIMARY KEY AUTO_INCREMENT,
    tipo VARCHAR(50) NOT NULL,
    descrizione TINYTEXT
);

CREATE TABLE IF NOT EXISTS Account (
    idAccount INT PRIMARY KEY AUTO_INCREMENT NOT NULL,
    nome VARCHAR(20) NOT NULL,
    cognome VARCHAR(20) NOT NULL,
    email VARCHAR(50) NOT NULL UNIQUE,
    password CHAR(64) NOT NULL,
    dataIscrizione DATE NOT NULL,
    dataScadenza DATE,
    tipoAccount INT NOT NULL,
    FOREIGN KEY (tipoAccount) REFERENCES TipiAccount (idTipoAccount)
);

CREATE TABLE IF NOT EXISTS Categorie (
    idCategoria INT PRIMARY KEY AUTO_INCREMENT,
    categoria VARCHAR(50) NOT NULL
);

CREATE TABLE IF NOT EXISTS Articoli(
    idArticolo INT PRIMARY KEY AUTO_INCREMENT,
    titolo TINYTEXT NOT NULL,
    sottotitolo TINYTEXT NOT NULL,
    testo TEXT NOT NULL,
    dataCreazione DATE NOT NULL,
    autore INT NOT NULL,
    FOREIGN KEY (autore) REFERENCES Account (idAccount),
    categoria INT NOT NULL,
    FOREIGN KEY (categoria) REFERENCES Categorie (idCategoria),
    hotwords TEXT
);

CREATE TABLE IF NOT EXISTS StatoRevisione (
    idStatoRevisione INT PRIMARY KEY AUTO_INCREMENT,
    stato VARCHAR(30) NOT NULL,
    descrizione TINYTEXT NOT NULL
);

CREATE TABLE IF NOT EXISTS Revisione (
    idRevisione INT PRIMARY KEY AUTO_INCREMENT,
    articolo INT NOT NULL,
    FOREIGN KEY (articolo) REFERENCES Articoli (idArticolo),
    stato INT NOT NULL,
    FOREIGN KEY (stato) REFERENCES StatoRevisione (idStatoRevisione)
);

